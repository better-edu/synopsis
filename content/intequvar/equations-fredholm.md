+++
title = "Интегральное уравнение Фредгольма"
weight = 7

[extra]
math = true
+++

## Метод последовательных приближений 

Если в уравнении Фредгольма 2-го рода
$$
y(x)=\lambda\int\limits_a^b K(x,t) y(t) dt+f(x)
$$ 
числовой параметр $\lambda$ удовлетворяет условию
$$
|\lambda| < \dfrac{1}{B},~\text{где}~B^2 = \int\limits_a^b\int\limits_a^b |K(x,t)|^2 dx dt
$$
то уравнение Фредгольма 2-го рода имеет единственное решение.
В этом случае оно может быть найдено методом последовательных приближений.
Выбрав произвольным образом нулевое приближение $y_0(x)$ можно построить последовательность функций $y_n(x)$
$$
y_1(x) = \lambda\int\limits_a^b K(x,t) y_0(t)dt + f(x) \\\\
y_2(x) = \lambda\int\limits_a^b K(x,t) y_1(t)dt + f(x),\\\\
\cdots \\\\
y_n(x) = \lambda\int\limits_a^b K(x,t) y_{n-1}(t)dt + f(x)
$$

Эта последовательность сходится к точному решению $y(x),$ то есть
$$\lim\limits_{n \rightarrow \infty} y_n(x) = y(x).$$

## Пример
Решить методом последовательных приближений интегральное уравнение
$$
y(x) = \sin \pi x + \dfrac{1}{2} \int\limits_0^1 y(t) dt
$$

**Решение.** 
В этом уравнении $\lambda = \dfrac{1}{2},$ а $K(x,t) = 1$
Поэтому
$$
B^2 = \int\limits_0^1\int\limits_0^1 |K(x,t)|^2 dx dt = \int\limits_0^1\int\limits_0^1 |K(x,t)| dx dt = 1
$$
и условие $|\lambda| < \dfrac{1}{B}$ выполнено. В качестве нулевого
приближения возьмем $y_0 = \sin \pi x$ и построим следующие приближения:
$$
y_1 (x) = \sin \pi x + \dfrac{1}{2} \int\limits_0^1 y_0(t) dt = \sin \pi x + \dfrac{1}{2} \int\limits_0^1 \sin \pi t dt = \sin \pi x + \dfrac{1}{\pi} \\\\
y_2 (x) = \sin \pi x + \dfrac{1}{2} \int\limits_0^1 y_1(t) dt = \sin \pi x + \dfrac{1}{2} \int \limits_0^1 \left(\sin \pi t + \dfrac{1}{\pi} \right) dt = \sin \pi x + \dfrac{1}{\pi} + \dfrac{1}{2 \pi} \\\\
y_3 (x) = \sin \pi x + \dfrac{1}{2} \int\limits_0^1 y_2(t) dt = \sin \pi x + \dfrac{1}{2} \int \limits_0^1 \left(\sin \pi t + \dfrac{1}{\pi} + \dfrac{1}{2 \pi} \right) dt =
\sin \pi x + \dfrac{1}{\pi} + \dfrac{1}{2 \pi} + \dfrac{1}{4 \pi}
$$

Вычислив несколько первых членов последовательно $\{y_n(x)\}$ замечаем,
что $n$-ое приближение может быть записано в следующем виде:
$$
y_n(x) = \sin \pi x + \dfrac{1}{\pi} + \dfrac{1}{2 \pi} + \dfrac{1}{2^2 \pi} + \cdots + \dfrac{1}{2^{n-1} \pi} = \sin \pi x+\dfrac{1}{\pi} \sum\limits_{k=0}^{n-1} \dfrac{1}{2^k}.
$$
Точное решение находим как предел
$$
y(x) = \lim_{n \rightarrow \infty} y_n(x) = \sin \pi x + \dfrac{1}{\pi} \sum\limits_{k=0}^{\infty} \dfrac{1}{2^k} = \sin \pi x + \dfrac{2}{\pi}.
$$

**Задание.** Решить интегральное уравнение методом последовательных
приближений.
$$
y(x) = \dfrac{1}{2} \int\limits_0^1 e^{x-t} y(t) dt + e^x
$$
