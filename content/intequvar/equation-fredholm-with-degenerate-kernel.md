+++
title = "Уравнение Фредгольма с вырожденным ядром"
weight = 8

[extra]
math = true
+++

$$
y(x) = \lambda \int\limits_a^b K(x,t)\,y(t)\,dt + f(x)
$$ 
с вырожденным ядром 
$$
K(x,t) = \sum\limits_{k=1}^n p_k(x)\,q_k(t)
$$ 
может быть сведено к системе алгебраических уравнений. Для этого перепишем уравнение в следующей форме:
$$
y(x) = \lambda \sum\limits_{k=1}^n p_k(x) \int\limits_a^b q_k(t)\,y(t)\,dt + f(x) = \lambda \sum\limits_{k=1}^n c_k\,p_k(x) + f(x)
$$
где числа 
$$
c_k = \int\limits_a^b q_k(t)\,y(t) dt
$$

Из выражения видно,
что решение $y(x)$ будет найдено как только будут определены все константы $c_k$. Подставим вместо функций $y(x)$ в интеграле выражение:
$$
c_k = \int\limits_a^b q_k(t) \left(\lambda \sum\limits_{i=1}^n c_i\,p_i(t) + f(t) \right) dt = \lambda \sum\limits_{i=1}^n c_i \int\limits_a^b p_i(t)\,q_k(t)\,dt + \int\limits_a^b q_k(t)\,f(t)\,dt = \lambda \sum\limits_{i=1}^n c_i\,a_{k i} + b_k
$$
где константы 
$$
a_{k i} = \int\limits_a^b p_i(t)\,q_k(t)\,dt, \quad b_k = \int\limits_a^b q_k(t)\,f(t)\,dt
$$
Теперь, вместо интегрального уравнения, мы имеем эквивалентную ему
систему линейных алгебраических уравнений 
$$
c_k - \lambda \sum\limits_{i=1}^n a_{k i}\,c_i = b_k
$$ 
относительно неизвестных чисел $c_k$ Решив систему и подставив $c_k$, получим решение
исходного интегрального уравнения.
Число решений интегрального уравнения с вырожденным ядром или его неразрешимость будут, таким образом, определяться свойствами алгебраической системы

## Примеры
Решить интегральное уравнение
$$
y(x) = \sin 2x + \int\limits_{-\pi}^{\pi} \left(\dfrac{1}{\pi} \sin x \sin t + t \right) y(t) dt
$$

*Решение.* Ядро данного интегрального уравнения вырожденное. Коэффициент
$\lambda$ примем равным 1. Обозначая
$$
p_1 (x) = \dfrac{1}{\pi} \sin x, \quad p_2(x) = 1, \quad q_1 (t) = \sin t, \quad q_2(t) = t
$$
найдем коэффициенты уравнений по формулам:
$$
a_{11} = \int\limits_{-\pi}^{\pi} \dfrac{1}{\pi} \sin^2 t \,dt = 1, \quad a_{12} = \int\limits_{-\pi}^{\pi} \sin tdt = 0\\\\
a_{21} = \int\limits_{-\pi}^{\pi} \dfrac{1}{\pi}\,t\,\sin t\,dt = 2, \quad a_{22} = \int\limits_{-\pi}^{\pi} t\,dt = 0 \\\\
b_1 = \int\limits_{-\pi}^{\pi} \sin t\,\sin 2t\,dt = 0, \quad b_2 = \int\limits_{-\pi}^{\pi} t\,\sin 2t\,dt = -\pi
$$

Система примет вид
$$
c_1 - c_1 = 0, \\\\
-2 c_1 + c_2 = -\pi
$$ 
Общим решением этой системы будет $c_1 = C, ~ c_2 = 2 C - \pi,$ где $C$ --- произвольная постоянная.
Следовательно, решением заданного интегрального уравнения будет любая
функция вида
$$
y(x) = \sin 2 x + C \cdot \dfrac{1}{\pi} \sin x + \left(2 C - \pi \right) \cdot 1 = \sin 2x - \pi + C \left(\dfrac{1}{\pi} \sin x + 2 \right)$$
с произвольной константой $C.$

## Задание
Решить или установить неразрешимость интегральных уравнений

$$\begin{array}{ll}
        \textbf{1.1} ~ y(x) = \int\limits_0^{\pi} \tg x \cos t y(t)dt + \cos x & \quad \quad \quad \quad \textbf{1.2} ~ y(x) = \int\limits_0^1 e^x t y(t) dt + e^{-x}.
    \end{array}$$
