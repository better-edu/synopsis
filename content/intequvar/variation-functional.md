# Вариация функционала 

Пусть дан некоторый класс $M$ функций $y(x)$ и задан функционал
$V = V[y(x)]$ на множестве $M$.

**Вариацией** или **приращением** $\delta y$ **аргумента** $y(x)$
функционала $V$ называется разность между двумя функциями $y(x)$ и
$y_1(x)$, принадлежащими выбранному классу $M$ функций:
$$\delta y = y_1 (x) -  y(x).$$

**Приращением функционала** $V$, отвечающим приращению $\delta y$
аргумента, называется величина
$$\Delta V = \Delta V[y(x)] = V[y(x) + \delta y(x)] - V[y(x)],$$ где
$$\delta y(x) = y_1(x) - y(x),~y(x) \in M, ~ y_1 (x) \in M.$$

Если приращение функционала $\Delta V$ можно представить в виде
$$\Delta V = L[y(x), \delta y] + \beta (y(x), \delta y)||\delta y||,$$
где $L[y(x), \delta y]$ --- линейный по отношению к $\delta y$
функционал и $\beta (y(x), \delta y) \to 0$ при $||\delta y || \to 0,$
то линейная по отношению к $\delta y$ часть приращения функционала, т.е.
$L[y(x), \delta y],$ называется **вариацией функционала** и обозначается
$\delta V.$ В этом случае функционал $V[y]$ называется
**дифференцируемым** в точке $y(x)$.

## Второе определение вариации функционала {#второе-определение-вариации-функционала .unnumbered}

Вариацией функционала $V$ в точке $y = y(x)$ называется значение
производной функционала $V[y(x) + \alpha \delta y(x)]$ по параметру
$\alpha,$ когда $\alpha = 0:$
$$\delta V = \left[\dfrac{d}{d \alpha} V\left[y(x) + \alpha \delta y(x)\right]\right]\Bigg|_{\alpha = 0}.$$

Если существует вариация функционала как главная линейная часть его
приращения, т.е. в смысле первого определения, то существует и вариация
как значение производной по параметру $\alpha$ при $\alpha = 0$ и эти
вариации совпадают.

::: {.pr}
Найти приращение функционала $V[y] = \int\limits_0^1 y(x) y'(x) dx,$
определенного в пространстве $C_1 [0,1],$ если
$y(x) = x, ~ y_1 (x) = x^2.$
:::

**Решение.**
$$\Delta V = V[x^2] - V[x] = \int\limits_0^1 x^2 \cdot 2x \cdot dx - \int\limits_0^1 x \cdot 1 \cdot dx = \int\limits_0^1 \left(2 x^3 - x \right) dx = 0.$$

::: {.pr}
Показать, что функционал $V[y] = \int\limits_a^b y(x) dx,$ заданный в
пространстве $C[a, b],$ дифференцируем в каждой точке $y(x)$ этого
пространства.
:::

**Решение.**
$$\Delta V = V[y+ \delta y] - V[y] = \int\limits_a^b [y(x) + \delta y(x)] dx - \int\limits_a^b y(x) dx = \int\limits_a^b \delta y(x) dx.$$

Таким образом, $\Delta V = \int\limits_a^b \delta y(x) dx.$ Это линейный
относительно $\delta y(x)$ функционал. В данном случае все приращение
$\Delta V$ свелось к линейному функционалу относительно $\delta y(x).$
Рассматриваемый функционал $V[y]$ дифференцируем в каждой точке $y(x)$ и
его вариация $$\delta V = \int\limits_a^b \delta y(x) dx.$$

::: {.pr}
Показать, что функционал $V[y] = \int\limits_a^b y^2(x) dx,$
определенный в пространстве $C[a, b],$ дифференцируем в каждой точке
$y(x).$
:::

**Решение.**
$$\Delta V = V[y+ \delta y] - V[y] = 2 \int\limits_a^b y(x) \delta y(x) dx + \int\limits_a^b [\delta y(x)]^2 dx.$$

Первый интеграл в правой части при каждой фиксированной функции $y(x)$
является линейным относительно $\delta y(x)$ функционалом. Оценим второй
интеграл в правой части. Имеем
$$\int\limits_a^b [\delta y(x)]^2 dx = \int\limits_a^b |\delta y(x)|^2 dx \leqslant \left(\max\limits_{a\leqslant x \leqslant b} |\delta y(x)|\right)^2 \int\limits_a^b dx = (b-a) ||\delta y(x)||^2 = (b-a) ||\delta y(x)|| \cdot ||\delta y(x)||.$$

При $||\delta y|| \to 0$ величина $(b-a) ||\delta y(x)|| \to 0.$
Следовательно, приращение $\Delta V$ функционала представимо в виде
суммы $L[y, \delta y]$ и добавки второго порядка малости относительно
$||\delta y||.$ По определению данный функционал является
дифференцируемым в точке $y(x)$ и его вариация
$$\delta V = 2 \int\limits_a^b y(x) \delta y(x) dx.$$

::: {.pr}
Пользуясь вторым определением, найти вариацию функционала
$V[y] = \int\limits_a^b y^2 (x) dx.$
:::

**Решение.** Вариация этого функционала в смысле первого определения
равна $\delta V = 2 \int\limits_a^b y(x) \delta y(x) dx$ (см. пример 3).
Найдем вариацию функционала $V[y],$ пользуясь вторым определением
вариации. Имеем
$$V[y(x) + \alpha \delta y(x)] = \int\limits_a^b [y(x) + \alpha \delta y(x)]^2 dx.$$

Тогда
$$\dfrac{d}{d \alpha} V[y+ \alpha \delta y] = 2 \int\limits_a^b (y+\alpha \delta y) \delta y dx$$
и, следовательно,
$$\delta V = \dfrac{d}{d \alpha} V[y+ \alpha \delta y]|_{\alpha = 0} = 2 \int\limits_a^b y \delta y dx.$$

Вариации функционала в смысле первого и второго определения совпадают.

::: {.pr}
Найти вариацию функционала $V = \int\limits_a^b F(x, y, y') dx.$
:::

**Решение.** Согласно второму определению вариации
$$\delta V = \left[\dfrac{d}{d \alpha} \int\limits_a^b F(x, y+ \alpha \delta y, y' + \alpha \delta y') dx \right] \Bigg|_{\alpha = 0}.$$
Здесь $\delta y'$ --- вариация производной $y'(x)$ аргумента $y(x).$
Вычисляя производную $V[y]$ по параметру $\alpha,$ получаем
$$\int\limits_a^b \left[F_y (x, y + \alpha \delta y, y' + \alpha \delta y') \delta y + F_{y'} (x, y+ \alpha \delta y, y' + \alpha \delta y') \delta y' \right] dx.$$

Полагая $\alpha = 0,$ находим вариацию функционала $V$
$$\delta V = \int\limits_a^b \left(F_y \delta y + F_{y'} \delta y' \right) dx.$$

::: {.pr}
Показать, что функционал $V = \int\limits_0^1 \left(x^2 + y^2\right) dx$
на кривой $y \equiv 0$ достигает строгого минимума.
:::

**Решение.** Для любой непрерывной на $[0,1]$ функции $y(x)$ имеем
$$\Delta V = V[y(x)] - V[0] = \int\limits_0^1 \left(x^2 + y^2 \right) dx - \int\limits_0^1 x^2 dx = \int\limits_0^1 y^2 dx \geqslant 0,$$
причем знак равенства достигается только при $y(x) \equiv 0.$

### Задание 1. {#задание-1. .unnumbered}

Найти $\Delta V,$ если
$V[y] = \int\limits_0^1 y y' dx, ~ y(x) = e^x, ~ y_1(x) = 1.$

### Задание 2. {#задание-2. .unnumbered}

Найти вариацию функционала. Решить, как минимум, один пример из
предложенных ниже. $$V[y] = \int\limits_a^b y y' dx;$$
$$V[y] = \int\limits_a^b (x+y) dx;$$
$$V[y] = \int\limits_a^b (y^2 - y'^2) dx;$$
$$V[y] = y^2(0) + \int\limits_0^1 (x y + y'^2) dx;$$ $$V\[y\] = \_0\^ y'
y dx.
