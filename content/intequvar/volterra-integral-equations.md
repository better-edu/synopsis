+++
title = "Интегральные уравнения Вольтерра"
weight = 10

[extra]
math = true
+++

## Метод последовательных приближений 

В отличие от уравнений Фредгольма, уравнения Вольтерра всегда имеют
единственное решение. Поэтому оно может быть найдено методом
последовательных приближений. Последовательность функций $y_n(x)$
строящаяся по
правилу
$$
\begin{array}{c}
y_1(x) = \lambda \int\limits_a^x K(x,t)y_0(t)dt + f(x)\\\\
y_2 (x) = \lambda \int\limits_a^x K(x,t)y_1(t)dt + f(x)\\\\
\cdots\\\\
y_n(x) = \lambda \int\limits_a^x K(x,t)y_{n-1}(t)dt + f(x)\\\\
\cdots
\end{array}
$$
всегда сходится к единственному решению интегрального уравнения при
$n \to \infty$

**Пример 1.** *Решить интегральное уравнение*
$$
y(x) = 1 - \int\limits_0^x (x-t)y(t)dt
$$
методом последовательных приближений.

*Решение.* В качестве нулевого приближения
выберем $y_0(x) = 1$
Тогда
$$
y_1(x) = 1 - \int\limits_0^x (x-t) \cdot 1 \cdot dt = 1 - \dfrac{x^2}{2}\\\\
y_2(x) = 1 - \int\limits_0^x (x-t) \left(1- \dfrac{t^2}{2}\right) dt = 1 - \dfrac{x^2}{2} + \dfrac{x^4}{4!}
$$
На $n$-м шаге получим
$$
y_n(x) = 1- \dfrac{x^2}{2} + \dfrac{x^4}{4!} - \dfrac{x^6}{6!} + \cdots + (-1)^n \dfrac{x^{2n}}{2n!} = \sum\limits_{k=0}^n (-1)^{k} \dfrac{x^{2k}}{2k!}
$$
откуда 
$$
y(x) = \lim\limits_{n \to \infty} y_n(x) = \sum\limits_{k=0}^{\infty} (-1)^{k} \dfrac{x^{2k}}{2k!} = \cos x
$$

**Задание 1. Решить уравнение Вольтерра методом последовательных приближений.**
$$
\begin{array}{ll}\textbf{1.1.} ~ y(x) = \int\limits_0^x y(s)\, ds + x^2 & \quad \quad \quad \quad \textbf{1.2} ~ y(x) = \int\limits_0^x y(s) ds + \dfrac{x^2} \\\\
\textbf{1.3.} ~ y(x) = \int\limits_0^x (x-s)\,y(s)\,ds + x & \quad \quad \quad \quad \textbf{1.4.} ~ y(x) = 1 - \int\limits_0^x \tg s\,y(s)\,ds \\\\
\textbf{1.5.} ~ y(x) = 1 + \int\limits_0^x \dfrac{y(s)}{x+s} ds & \quad \quad \quad \quad \textbf{1.6.} ~ y(x) = 2\int\limits_0^x s\,y(s)\,ds + x^2
\end{array}
$$

## Уравнения Вольтерра с вырожденным ядром 

Если ядро интегрального уравнения Вольтерра является вырожденным, то это
уравнение может быть представлено в
виде:
$$
y(x) = \sum\limits_{k=1}^n p_k(x) \int\limits_a^x q_k(t)\, y(t)\,dt + f(x)
$$
Вводя функции

$$
u_k(x) = \int\limits_a^x q_k(t)\,y(t)\,dt, \quad k =1, 2, ... , n
$$
и подставляя их в уравнение получим, что решение интегрального уравнения с вырожденным ядром имеет вид
$$
y(x) = \sum\limits_{k=1}^n p_k(x)\,u_k(x) + f(x)
$$

Таким образом, чтобы найти $y(x)$ необходимо определить функции $u_k(x)$
Продифференцировав соотношения, и подставив вместо $y(x)$ выражение, получим систему дифференциальных уравнений 1-го порядка для неизвестных функций

$$
u_k(x): u\'_k(x) = \sum\limits_{i=1}^n q_k(x)\,p_i(x)\,u_i(x) + f(x) q_k(x),k=1,2,...,n
$$

Положив $x=a$ в соотношениях, найдем, что начальные условия являются однородными:
$u_1(x) = u_2(x) = ... =u_n(x) = 0$ 
Подстановка решения системы дифференциальных уравнений даст решение исходного интегрального
уравнения

**Пример 2.** *Решить интегральное уравнение*
$$
y(x) = \int\limits_0^x \dfrac{\ch s}{\ch x}y(s)ds + 1
$$
*Решение.*
Обозначим
$$
u(x) = \int\limits_0^x \ch sy(s)ds
$$
Тогда уравнение  перепишется в виде
$$
y(x) = \dfrac{u(x)}{\ch x} + 1
$$
Продифференцируем и подставим вместо
$y(x)$ выражение, получим
$$
u'(x) = \ch x\,y(x) = \ch x \left(\dfrac{u(x)}{\ch x} + 1\right) = u(x) + \ch x
$$
или в стандартной форме
$$
u' - u = \ch x
$$
Решением этого уравнения с учетом начального условия $u(0) = 0$ будет функция
$$
u(x) = \dfrac{1}{2} ( x e^x + \sh x)
$$
Подставляя ее в получим решение интегрального уравнения:
$$
y(x) = 1 + \dfrac{1}{2} \dfrac{x e^x + \sh x}{\ch x}
$$

**Задание 2.** 
*Решить уравнение Вольтерра с вырожденным ядром.*
$$\begin{array}{ll}\textbf{2.1} ~ y(x) = \int\limits_1^x \dfrac{2 s}{x^2}y(s)ds + x^2 & \quad \quad \quad \quad \textbf{1.2} ~ y(x) = 2 \int\limits_0^x \dfrac{y(s)}{2s+1} ds + 4x \\\textbf{2.3} ~ y(x) = \int\limits_0^x \dfrac{\sin x}{\cos s} y(s) ds + 1 & \quad \quad \quad \quad \textbf{1.4} ~ y(x) = \int\limits_1^x \dfrac{x \cos x}{s \cos s} y(s) ds + \cos x e^x \\\textbf{2.5} ~ y(x) = 1 + \int\limits_\pi^x \dfrac{x^2}{s^3} y(s) ds + x^3 \cos x & \quad \quad \quad \quad \textbf{1.6} ~ y(x) = \int\limits_e^x \dfrac{2}{s \ln x} y(s) ds + 1.\end{array}$$
