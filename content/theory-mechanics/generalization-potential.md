+++
title = "Обобщенный потенциал и силы"
+++

$$
L = E - U 

U = U(r,\dot{r},t)
d/dt\frac{\partial L}{\partial \dot{r}}-\frac{\partial L}{\partial \dot{r}}=0
$$


## Обощенная потенциальная сила
$$
\mathcal{F} = -\nabla u + d/dt\frac{\partial u}{\partial \dot{t}}
$$


## Потенциальная энергия заряда во внешнем потенциальном поле

E = E(r,t)
F = F(r,t)
F = qE+q/c[\dot{r}\time \vec{M}]

Возникают еще потенциалы

$$
\phi(r,t), \vec{A}(r,t) \\\\
E = -\nabla\phi - 1/c\frac{\partial A}{\partial t} \\\\
H = [\nabla\times A] \\\\
F = qE + [\dot{r}\times[\nabla\times A]]
$$

## Механические системы со связями. Голоновные связи. Обоющенные координаты

$$
F(x,y,z,t) = 0 \\\\
|r_2-r_| = l_1 \\\\
|r_2-r_1| = l_2
$$

Связь
$$
F(r_1,r_2,\ldots,r_N,\dot{r_1},\ldots,\dot{r_N},t) = 0 \\\\
\tilde{F}(\{r\},t) = 0
$$

Метод Лагранжа, устронение голоновных связей

$$
S = 3N - K
q = \{q_1,\ldots,q_s\}
$$

### критерии для выбора обобщенных независимых координат
1. Преобразование должно быть невырожденное $(det\neq 0)$.
2. Все уравнения связи должны обращаться в 0

На пример сферический маятник


## Уравнения в обобщенных координатах

$$
L = L(q_{\alpha},\dot{q}_{\alpha},t)
S = \int_{t_1}^{t_2}{L(q_{\alpha},\dot{q}_{\alpha},t)dt}
\frac{d}{dt}(dl/d\dot{q}) - \frac{\partial L}{\partial q_{\alpha}} = Q
$$

## Физическая и диссипативная энергия в обобщеных координатах

$$
T = \frac{1}{\dot{r}}{\sum{m_i}\dot{r}_i^2} \\\\
D = \frac{1}{\dot{r}}{\sum{k_i}\dot{r}_i^2} \\\\
df/dt = \cos{(x^2(t)\cdot 2\cdot x(t)\cdot \dot{x(t)t^2})}+2t\sin{x^2t} \\\\

T = 1/r\sum{m_i}(\frac{dr_i}{dt})^2 \\\\
T = T^{<2>}+T^{<1>}+T^{<0>}
$$
> квадратичная форма, линейная комбинация

$$
T = T^{<2>} \\\\
D = D^{<2>} \\\\

T^{<2>} = \sum{a_{\alpha\beta}{\dot{q}_{\alpha}\dot{q}_{\beta}}}
$$

Обобщенный импульс
$$
\frac{\partial L}{\partial \dot{q}} = P_{\alpha}
$$

Циклическая координата
$$
\frac{\partial L}{\partial q_{\alpha}} = 0
$$


Ообщенный импульс это момент импульса, если обощенная координата угол

### Обобщенная энергия

$$
\frac{d}{dt}\frac{\partial L}{\partial \dot{q}}-\frac{d}{dt}\frac{\partial L}{\partial q}= Q_{\alpha}^{d} \\\\

L = L(q_{\alpha},\dot{q_{\alpha}},t)
$$

После взятие суммы

$$
\frac{d}{dt}\sum{\alpha}{p_{\alpha}\dot{q_{\alpha}}} - L = \dot{H} = -\frac{dL}{dt}+\sum{Q_{\alpha}\dot{q_{\alpha}}}
$$

получим обобщенную энергию

### На пример материальная точка
### Заряд в электромагнитном поле
