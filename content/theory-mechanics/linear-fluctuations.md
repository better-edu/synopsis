+++
title = "Линейные колебания. Собственные частоты"
+++

Равновесии положение когда не действуют силы

При выводе системы из положения равновесия, возникают силы которые систему возвращают в положение равновесия

$$
F(x) = 0

\frac{\partial u}{\partial x} = 0
$$

Посмотрим как действуют силы $x_1\pm dx$

$$
F(x_1+dx)=-\frac{\partial u}{\partial x}\vert{x_1+dx} > 0
F(x_1-dx)=-\frac{\partial u}{\partial x}\vert{x_1-dx} < 0
$$

Система смещается вправо(влево), силы уводят от положение равновесия - оно нейстойчиво

$$
F(x_1+dx)=-\frac{\partial u}{\partial x}\vert{x_1+dx} 0
F(x_1-dx)=-\frac{\partial u}{\partial x}\vert{x_1-dx} > 0
$$

Здесь силы к положению равновесия, там силы устойчивое

Если связи голоновные, стационарные  то
$$
U(q)=U(q_1,\ldots,q_3)
$$

Условия минимума(достаточное положение равновесия)
$$
dU = 0 \\\\
d^2U > 0
$$

$$
dU = \sum_{\alpha}{\frac{\partial u}{\partial q_{\alpha}}dq_{\alpha}} = 0 \\\\
d^2U = \sum_{\alpha,\beta}{\frac{\partial^2 u}{\partial q_{\alpha}\partial q_{\beta}}dq_{\alpha}}dq_{\beta}  > 0
$$

Симметричную матрицу можно привести к диагональной форме

## Функция лагранжа в приближении линейных колебаний
Рассматриваем систему из предыдущего пункта

$$
\xi_{\alpha}= q_{\alpha}-q_{\alpha}^{(eq)}
;\quad \xi_{\alpha} \ll L
$$

Малы они конечно в некотором смысле. Скорости тоже малы
$$
L(q) = T_{q}-U_q \\\\
U(q)\approx U(q^{(eq)})+1/1!\sum_{\alpha}{\frac{\partial u}{\partial q_{\alpha}}}+1/2!\sum_{\alpha,\beta}{\frac{\partial^2 u}{\partial q_{\alpha}\partial q_{\beta}}dq_{\alpha}}+\ldots=\\\\
1/2\sum_{\alpha,\beta}{C_{\alpha,\beta}\xi_{\alpha}\xi_{\beta}}
$$

Это потенциальная энергия. Кинетическая

$$
T(u) = 1/2\sum^{s}_{\alpha,\beta}{A(q^{eq})_{\alpha,\beta}\dot{\xi_{\alpha}}\dot{\xi_{\beta}}}
$$

при малых колебаний. Аналогично диссипативные потенциал

Тогда 
$$
L = \sum_{\beta}{a_{\beta\alpha}\dot{\xi_{\beta}}}
$$

Гармонический осциллятор
$$
\ddot{\theta_{\alpha}}+\omega^2_{\alpha}\theta = 0
$$

Нормальные координаты $\theta$, а $\omega$ собственные частоты

