+++
title = "Задача двух тел"
draft = true
weight = 11
+++

11. Задача двух тел.

$$
m_1r_1(t)=F_{12} \\\\
m_1\ddot{r_1(t)}=F_{21} \\\\
R_c = \frac{m_1 r_1 + m_2 r_2}{m_1+m_2} \\\\
M = m_1+m_2 \\\\
M\ddot{R_c} = F_{12}(r,\dot{r})+F_{21}(r,\dot{r})=0 \\\\
\dot{R_c} = v_c \\\\
R_c = R_c(0)+v_c t \\\\
M R_c = 0 \\\\ 
r_1 = -\frac{m_2 r_2}{m_1} \\\\
r_2 - r_1 = r'_2-r'_1 \\\\
\ddot{r} = - \frac{m_1+m_2}{m_1m_2}F_{12} \\\\
$$
Приведенная масса
$$
\nu = \frac{m_1m_2}{m_1+m_2} \\\\
\nu\ddot{r} = F_{12} \\\\
$$

Движение фиктивной точки
$$
r = r_2 - r_1 = -\frac{m_1}{m_2}r'_1 - r'_1
r_2 = R_c + \frac{m}{M}\vec{r}
r_1 = R_c - \frac{m}{M}\vec{r}
$$

Система луна-земля

$$
r_e = - \frac{m_{m}}{m_e+m_m}\vec{r}
r_m = \frac{m_{e}}{m_e+m_m}\vec{r}
$$

## Теорема о вириале

Система точек финитная(не какие  точки не уходят на бесконечность)
$$
|r_i| \lt R_{max} \\\\
|p_i| \lt p_{max}
$$

Вириал 
$$
|r_i|\cdot |p_i|
$$

Найдем производную
$$
d/dt(r_i\cdot p_i) = v_i p_i + r_i F_i \\\\
(v_i,p_i) = d/dt(r_i\cdot p_i)-(r_i F_i) \\\\
2E_i = d/dt(r_i\cdot p_i)-(r_i F_i) \\\\
\sum{2E_i} = \sum{d/dt(r_i\cdot p_i)-(r_i F_i)}
$$

Если мы усредним последнее выражение
$$
2\bar{E_i} = (\lim_{t\to \infty}{1/E\int_{0}^{E}d*dt/dt{\sum_{i}{r_i p_i}}}=0) - \sum_i(r_iF_i) \\\\
2\bar{E_i} = - \sum_i(r_iF_i) \\\\
2 E_i = \sum{r_i \frac{\delta U}{\delta r_i}}
$$

Теорема Эйлера
$$
\sum_{x_j\frac{\partial U}{\partial x_j}} = kU
$$

По этой теореме
$$
2\bar{E}=k\bar{U}
$$
