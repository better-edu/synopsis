+++
title = "Связанные маятники"
+++

```
--------
| \    \
|  \    \
|   \    \
|    \    \
|     \    \
|      \    \
\/
```


$$
L = m/2(\dot{x^2_1}+\dot{x^2_2}+\dot{y^2_1}\dot{y^2_2})+mgx-k/2[(x_1-x_2)^2+(y_2-y_1-a)^2]
$$

Частота маятника

$$
2L/ml^2 = \dot{\varphi^2_1}+\dot{\varphi^2_2}-g/L(\varphi^2_1+\varphi^2_2)-k/m(\varphi^2_1-\varphi^2_2)
$$

В матричной форме

$$
L = \dot{\Phi}^{T}A\dot{\Phi}-(\Omega^2_1+\Omega^2_2)\Phi^{T}C\Phi \\\\
L = \dot{\Phi}^{T}\dot{\Phi}-(\Omega^2_1+\Omega^2_2)\Phi^{T}C\Phi \\\\
\Theta = R\Phi \\
L = \dot{\Theta^2_1}-(\Omega^2_1+\Omega^2_2)(1-a)\Theta^2_1+\dot{\Theta^2_1}-(\Omega^2_1+\Omega^2_2)(1-a)\Theta^2_2
$$

Биение не соотвествуют смещению координат

## Первую форму к диагональной форме
