+++
title = "Кинематика материальной точки"
weight = 2

[extra]
math = true
+++

Положение материальной точки дает радиус вектор

$$
\vec{r}=x\cdot \vec{n_x} +y\cdot \vec{n_y} +z\cdot \vec{n_z} = (x(t),y(t),z(t))
$$

## Закон движения

$$
\vec{r}=\vec{r}(t)
$$

## Скорость материальной точки

$$
\vec{v}=\frac{d\vec{r}(t)}{dt} = \dot{\vec{r}}
$$

## Ускорение

$$
\vec{w}=\frac{d\vec{v}(t)}{dt} = \ddot{\vec{r}}
$$

Траектория - кривая в пространстве

## Основная задача кинематике

определить закон движение тела, скорость движение тела, ускорение

$$
F(x,y) = 0 \\\\ \text{плоская}
F(x,y,z) = 0; G(x,y,z)= 0 \text{пространственная}
$$

## Секторная скорость

$$
\vec{\sigma} = 1/2[\vec{r}(t)\times \vec{v}(t)]
$$

### Постоянная

$$
\vec{\sigma} = (\vec{\sigma_0}\cdot \vec{r})=\vec{\sigma_{0x}}x(t)+\vec{\sigma_{0y}}y(t)+\vec{\sigma_{0z}}z(t)
$$

> последние похоже на уравнение плоскости

### Смысл

$$
\vec{\sigma} = 1/2[\vec{r}(t)\times \frac{\vec{dr}}{\vec{dt}}]
$$

Площадь
![trianlge sigma](https://raw.re128.net/kogeletey/screenshots/981.jpg)

$$
|\vec{\sigma}| = 1/2 |\vec{r}|\cdot  \frac{|\vec{dr}|}{\vec{dt}}\sin{\alpha}= \frac{dS}{dt} = |\vec{S}|
$$

секторная скорость заметающая площадь

### Второй закон Кеплера

![kepler](https://raw.re128.net/kogeletey/screenshots/21303.jpg)
