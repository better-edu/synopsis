+++
title = "Преобразование лапласа и его свойства"
weight = 16

[taxonomies]
authors = ["kogeletey"]

[extra]
math = true
+++

### 16.  Преобразование Лапласа и его свойства.

Ставит соотношения в соответствие действительной переменной функцию комплексной переменной с помощью интеграла.

$$
F(p)=\int_{0}^{\infty}{e^{-pt}f(t)dt}
$$

При этом $f(t)$ должна обладать условиями

1. $t\lt 0$ функция $f(t)\equiv 0$
2. $t\geqslant 0$ имеет не более конечного числа разрывов точек первого рода
3. $t\to \infty$ имеет ограниченную степень роста, тоесть

$$
|f(t)|\leqslant Me^{at}
$$

## Преобразование Хевисайда
$$
\tilde{F}(p)=p\int_{0}^{\infty}{e^{-pt}f(t)dt}
$$

## Свойства

Изображение Лапласа функции является аналитической функцией комплексной переменной $p$ в области $\operatorname{Re}{p}\gt a$, где $a$ показатель степени роста

1. Линейность

2. $F(p)\fallingdotseq f(t)$, $Re\\ p\gt \alpha$ то 

$$
\frac{1}{\alpha}F{(p/\alpha)}\fallingdotseq f(\alpha t)
$$

3. Запаздывание.
Заданы предыдущие условия и задана функция 
{% katex(block=true) %}
f_{\tau}(t) = \left\{ \begin{array}{cl}
0, t\lt\tau, \tau\lt 0 \\
f(t-\tau), t\leqslant\tau
\end{array} \right.
{% end %}

Тогда 

$$
f_{\tau}(p)\fallingdotseq F_{\tau}(p)=e^{-p\tau}F(p)
$$

4. Если $f^{(n)}(t)$ удовлетворяет условием существования изображения, то

$$
f^{(n)}(t)\fallingdotseq p^n\left(F(p)-\sum{\frac{f^{k-1}(0)}{f^k}}\right)
$$

5. Пусть $f(t)\fallingdotseq F(p)$, тогда

$$
\prod_{k=0}^{n}{\int_{0}^{t_{k}}{dt_k}}\int_{0}^{t_{n-1}}{f(t_n)dt_n}\fallingdotseq \frac{1}{p^n}F(p)
$$

6. Сверткой называется функция, определенная соотношением

$$
\varphi(t) = \int_{0}^{t}{f_1(\tau)f_2(t-\tau)d\tau}=\int_{0}^{t}{f_1(t-\tau)f_2(\tau)d\tau}
$$

А значит имеет место следующие
$$
\varphi(t) = \int_{0}^{t}{f_1(\tau)f_2(t-\tau)d\tau}\fallingdotseq F_1(p)F_2(p), Re p\gt \max{\\{a_1,a_2\\}}
$$

7. Дифференцирование

$$
F^{(n)}\fallingdotseq  (-1)^n t^n f(t)
$$

8. Интегрирование.

Функция $\frac{f(t)}{t}$ удовлетворяет условиям существования изображения.

$$
\frac{f(t)}{t}\fallingdotseq \int_{0}^{\infty}{e^{-pt}\frac{f(t)}{t}dt}=\int_{p}^{\infty}{F(q)dq}
$$

9. Теорема смещения. Для любого комплексного числа $\lambda$. 

$$
F(p+\lambda)\fallingdotseq e^{-\lambda t}f(t) dt
$$

