+++
title = "Магнитное поле на оси кругового тока"
weight = 26

[taxonomies]
authors = ["kogeletey"]

[extra]
math = true
+++

### 26\. Закон Био-Савара-Лапласа. Магнитное поле кругового тока.
Ниже на картинке показан $d\vec{B}$ от тока $I dl$, находящейся справа.
От всех элементов будет образовываться конус векторов и они будут направлены по оси $Z$
Значит достаточно сложить проекции векторов $d\vec{B}$

$$
dB_z=dB\cos{\beta}=\frac{\mu_0}{4\pi}\frac{I dl}{r^2}\cos{\beta}
$$

Интегрируя все выражение по $dl$ и делая замену на $\cos{\beta}=R/r$ и $r=(z^2+R^2)^{1/2}$

$$
B=\frac{\mu_0}{4\pi}\frac{2\pi R^2 I}{(z^2+R^2)^{3/2}}
$$

В центре витка с током и на расстоянии $z\gg R$ модуль вектора равен

$$
B_{z=0}=\frac{\mu_0}{4\pi}\frac{2\pi I}{R}, B_{z\gg R}\approx \frac{\mu_0}{4\pi}\frac{2\pi R^2 I}{z^3}
$$

![axes circular current](https://raw.re128.net/kogeletey/knw-media/electromagnetism/axes-circular-current.png)
