+++
title = "Природа электрического тока"
weight = 21

[taxonomies]
authors = ["kogeletey"]

[extra]
math = true
+++


### 21\. Классификация твердых тел (проводники, диэлектрики, полупроводники). Природа электрического тока в металлах. Опыты Рикке, Мандельштама, Папалекси, Толмена и Стюарта.

## Удельная проводимость
обозначается $\lambda$ 
- металлов $6\cdot 10^3\ldots 6\cdot 10^5 Ом^{-1} м^{-1}$
- полупроводников $10^4\ldots  10^{-10} Ом^{-1} м^{-1}$
- диэлектриков $10^{-10}\ldots 10^{-20} Ом^{-1} м^{-1}$

## Природа носителей в металлах

Был установлен ряд опытов. 
Например, опыт Рикке, который взял три цилиндра и один алюминиевый. 
Цилиндры были сложены в последовательность медь-алюминий-медь. 
Через такой проводник пропускался ток в течении года, где выяснилось что ток не влияет на массу цилиндра.
Также не было обнаружено проникновение одного металла в другой, так как заряд переносится электронами.

Для отождествления носителей тока, нужно было определить знак и величину удельного заряда.
Опыт был поставлен, на основании того что в металле легко перемещающиеся заряженные частицы, то при торможении эти частицы должны двигаться с некоторой
скоростью. 
- $v_0$ - скорость с которой движется в начале проводник
- $w$ - ускорение торможения

![accelerated conductor](https://raw.re128.net/kogeletey/knw-media/electromagnetism/accelerated-conductor.jpg)

Ускорение можно сообщить  и не подвижном проводнике, если создать поле напряженности $E=-\frac{m w}{e'}$ приложить к концам проводника разность потенциалов
$U=l E=-\frac{m w l}{e'}$.
Тогда потечет ток силой $I=\frac{U}{R}$
Значит за время $dt$ через каждое сечение пройдет заряд

$$
dq=i dt=-\frac{m w l}{e' R}dt=-\frac{m l}{e' R}dv
$$

За все время пройдет заряд
$$
q=\int_{0}^t{dq}=-\int_{v_0}^0{\frac{ml}{e' R}dv}=\frac{m l v_0}{e' R}
$$

Все величины измеряемы, значит можно найти заряд. Направление импульса даст знак.

Первые опыты проводились с помощью телефона, в котором был слышен звук импульсов токов.
Для количественно измерения использовались гальванометры.

Вычисленное по формуле значение удельного заряда, получились очень близкими к $e/m$ для электронов.
Так как ток в металлах можно вызвать весьма малой разностью потенциала, то это значит что электроны движутся свободно.



