+++
title = "Работа сил поля"
weight = 6

[taxonomies]
authors = ["kogeletey"]

[extra]
math = true
+++

### 6. Работа сил поля при перемещении зарядов. Циркуляция вектора напряженности. Потенциальный характер электростатического поля. Разность потенциалов. Электрический потенциал и его нормировка. Эквипотенциальные поверхности.

## Работа по перемещению заряда
На положительный точечный заряд $q$ действует сила $\vec{F}=q\vec{E}$.
При перемещении заряда на отрезке $d\vec{l}$ силами поля совершается работа

$$
dA=\vec{F}d\vec{l}=q Edl\cos{(\vec{E}\cdot d\vec{l})}
$$

При перемещении заряда $q$ из точки 1 в точку 2, работа равна
$$
A=q\int_{1}^{2}{\vec{E}d\vec{l}}
$$

## Интеграл по замкнутому контуру называется циркуляцией.

Работа, совершаемая силами электрического поля при перемещении единичного положительного заряда, определяется как циркуляция вектора напряженности 
электрического поля при перемещении единичного положительного заряда по замкнутому контуру длиной $l$, определяется как циркуляция вектора напряженности электрического поля.

$$
\oint_{l}{E dl\cos{(\vec{E}\cdot d\vec{l})}}=\oint_{l}{\vec{E}d\vec{l}}
$$

Для замкнутого пути положение начальной и конечной точек перемещение заряда совпадает, то работа сил равна нулю

$$
\oint_{l}{\vec{E}d\vec{l}}=0
$$

> циркуляция вектора напряжённости электрического поля по контуру равна нулю

## Определение потенциального характера электростатического поля (или определение электростатического поля)

![potential character](https://raw.re128.net/kogeletey/knw-media/electromagnetism/field-work-force/potential-character.png)

Проекция $d\vec{l}$ на $\vec{r}$ равна $dr=dl\cos{\alpha}$


Составим закон Кулона 
$$
\vec{F}(\vec{r})=\frac{1}{4\pi\varepsilon_0}\frac{Q q\vec{r}}{r^3}
$$

Работа поля зависит только от начального $r_1$ и конечного $r_2$ положение заряда $q$

{% katex(block=true) %}
$$
A_{12}=\int_{r_1}^{r_2}{\frac{1}{4\pi\varepsilon_0}\frac{Q q\vec{r}}{r^3}d\vec{l}}=
\frac{1}{4\pi\varepsilon_0}\int_{r_1}^{r_2}{\frac{Q q\cos{\alpha r}}{r^3}dl}= \\
\frac{1}{4\pi\varepsilon_0}\int_{r_1}^{r_2}{\frac{Q q r}{r^3}dr}= \\
\frac{Q q}{4\pi\varepsilon_0}\left(\frac{1}{r_1}-\frac{1}{r_2}\right)
$$
{% end %}


Потенциал электростатического поля $\varphi$ в данной точке (точке наблюдения) с радиус-вектором $\vec{r}$ 
- отношение потенциальной энергии $U$ неподвижного пробного положительного заряда $q_0$ в поле сил Кулона, помещённый в данную точку с радиус-вектором, к величине этого заряда

$$
\varphi(r)=\frac{U(r)}{q_0}
$$


Единицей потенциала является вольт В: 1В – потенциал такой точки поля, в которой заряд в 1 Кл обладает потенциальной энергией 1 Дж.


Потенциал, как и потенциальная энергия, определяется с точностью до произвольной постоянной $C$, которую можно определить, если потенциалу произвольной точки поля приписать конкретное значение.
За нулевой потенциал удобно принимать потенциал бесконечно удалённой точки пространства, но это в теории. 
На практике за нулевой потенциал обычно принимают потенциал Земли.
Потенциал поля – всегда непрерывная функция координат.

## Разность потенциалов между любыми точками электростатического поля равна:

$$
\varphi_1 - \varphi_2 = \frac{A_{12}}{q_0} = \int_{1}^{2}{\vec{E}d\vec{l}}
$$

$q_1$ – потенциал электрического поля в точке 1,

$q_2$ – потенциал электрического поля в точке 2,

$A_{12}$ – работа сил электрического поля по перемещению пробного заряда $q$ из точки 1 к точке 2.

$d\vec{l}$ – бесконечно малый вектор, который направлен по касательной к траектории движения пробного заряда $q_0$

## Эквипотенциальная поверхность 

поверхность, на которой потенциал остается постоянным. 
Линии напряженности поля перпендикулярны к эквипотенциальным поверхностям и направлены в сторону убывания потенциал

![power lines](https://raw.re128.net/kogeletey/knw-media/electromagnetism/field-work-force/power-lines.png)

Рис. 1. Силовые линии (сплошные линии) и поперечное сечение в плоскости рисунка эквипотенциальных поверхностей (пунктирные линии) для системы из двух разноимённых точечных зарядов.

