+++
title = "Первые дифференциальные уравнения"
weight = 2
date = "2021-12-12"

[taxonomies]
authors = ["kogeletey"]

[extra]
math = true
+++

### 2.  Простейшие виды дифференциальных уравнений первого порядка (с разделяющимися переменными, однородные, в полных дифференциалах) и методы их решения.

# Простейшие ОДУ, интегрируемые в квадратурах

## Уравнение с разделящимися переменными

$$
\frac{dy}{dx}=\frac{f_1(x)}{f_2(x)},f_2(y)\neq 0 \\\\
f_1(x)dx=f_2(y)dy \\\\
\int{f_1(x)dx}-\int{f_2(y)dy}=C
$$

$f_1,f_2$- непрерывные функции

1 - интеграл

## Автономные уравнения движения мат. точки на прямой

$$
m\frac{d^2x}{dt^2}=f(x) \\\\
\frac{dx}{dt}\frac{d^2x}{dt^2}=\frac{1}{m}f(x)\frac{dx}{dt}\\\\
\frac{d}{dt}\left[\left(\frac{dx}{dt}\right)^2\right]=\frac{2}{m}\frac{d}{dt}\left[\int{f(x)dx}\right] \\\\
\left(\frac{dx}{dt}\right)^2-\frac{2}{m}\int{f(x)dx}=c_1 \\\\
\frac{dx}{dt}=\pm\sqrt{\frac{2}{m}\int{f(x)dx}+c_1} \\\\
dt=\pm\frac{dx}{\sqrt{\frac{2}{m}\int{f(x)dx}+c_1}}
$$

Уравнение в квадратурах

$$
t+c_2=\int{\frac{dx}{\sqrt{\frac{2}{m}{f(x)dx}+c_1}}}
$$

общий интеграл

### Динамическая система

- эквивалетная уравнению движения
  $$
  \begin{rcases}
  x=x_1; \frac{dx_1}{dt}=x_2 \\\\
  \frac{dx_2}{dt}=\frac{1}{m}f(x_1)
  \end{rcases}
  $$

## Фазовая плоскость, траектория

уравнение(или система)

$$
\frac{dx_2}{dx_1}=\frac{f(x)}{m x_2} \\\\
x_2dx_2={1\over m}f(x_1)dx_1 \\\\
V(x)=V(x_1,x_2)=(x_2)^2-{2\over m}\int{f(x_1)dx_1}
$$

# Линейное уравнения

## Линейное дифференциальное уравнение 1-го порядка

$$
\frac{dy}{dx}+p(x)y=f(x)
$$

## Неоднородное уравнение

$$
x\in X:p(x),f(x)\in C(X)
$$

## Однородное уравнение

$$
\frac{dy}{dx}+p(x)y=0 \\\\
\ln{y}+\int{p(x)dx}=c_1 \\\\
\implies y(x)=ce^{-\int{p(x)dx}}
$$

разделение переменных$(y\neq 0)$

### Представление уравнения выше - общее решение

Пусть $\varphi(x)$-решение(некоторое) Рассмотрим функцию

$$
\Phi(x)=\varphi(x)e^{\int{p(x)dx}} \\\\
\frac{d\Phi}{dx}=e^{\int{p(x)dx}}\left(\frac{d\varphi}{dx}+p(x)\varphi(x)\right) \\\\
\implies\Phi(x)=c
$$
