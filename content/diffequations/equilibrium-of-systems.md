+++
title = "Положения равновесия линейных автономных систем(24)"
weight = 24
date = "2021-12-12"

[taxonomies]
authors = ["kogeletey"]

[extra]
math = true
+++

### 24. Траектории в окрестности точки покоя. Типы точек покоя. Фазовый портрет линейной системы на плоскости.

## Траектория

Матрица 

$$
\frac{dx}{dt}=A x
$$

обладает тривиальным решением, и здесь есть состояние покоя.

## Фазовый портрет

- [Положения равновесия линейных автономных систем](http://www.math24.ru/%D0%BF%D0%BE%D0%BB%D0%BE%D0%B6%D0%B5%D0%BD%D0%B8%D1%8F-%D1%80%D0%B0%D0%B2%D0%BD%D0%BE%D0%B2%D0%B5%D1%81%D0%B8%D1%8F-%D0%BB%D0%B8%D0%BD%D0%B5%D0%B9%D0%BD%D1%8B%D1%85-%D0%B0%D0%B2%D1%82%D0%BE%D0%BD%D0%BE%D0%BC%D0%BD%D1%8B%D1%85-%D1%81%D0%B8%D1%81%D1%82%D0%B5%D0%BC.html)

## Зачем нужно?

Для построения фазовых портретов.

## Как применять

Если матрица системы линейных дифференциальных уравнений не вырождена, то

| Точка равновесия | Собственные значения $\lambda_1$,$\lambda_2$ |
| - | - |
| Узел  |	$\lambda_1,\lambda_2$ - действительные числа одного знака ($\lambda_1\cdot\lambda_2\gt 0$) |
| Седло	| $\lambda_1,\lambda_2$ - действительные числа разного знака ($\lambda_1\cdot\lambda_2\lt0$) |
| Фокус	| $\lambda_1,\lambda_2$ - комплексные числа; действительные части равны и отличны от нуля ($\operatorname{Re}{\lambda_1}=\operatorname{Re}{\lambda_2}\neq 0$) |
| Центр |	$\lambda_1,\lambda_2$ - чисто мнимые числа ($\operatorname{Re}{\lambda_1}=\operatorname{Re}{\lambda_2}=0$) |

Седло является нейстойчивой точкой

### 1. Если корни различны ($\lambda_1\neq \lambda_2$) 

Решаем уравнение

$$
x=C_1 a_1e^{\lambda_1 t} + C_2 a_2e^{\lambda_2 t}
$$

#### отрицательны($\lambda_1,\lambda_2\lt 0$) 

![steady knot](https://raw.re128.net/kogeletey/knw-media/diffequations/stable-node.jpg)

#### положительны($\lambda_1,\lambda_2\gt 0$)

![unstable knot](https://raw.re128.net/kogeletey/knw-media/diffequations/unstable-node.jpg)

### 2. Если корни $\lambda_1=\lambda_2=\lambda \lt 0$

![stable dicritical node](https://raw.re128.net/kogeletey/knw-media/diffequations/stable-dicritical-node.jpg)

#### корни $\lambda \gt 0$

![unstable dicritical node](https://raw.re128.net/kogeletey/knw-media/diffequations/unstable-dicritical-node.jpg)

### 3. Вырожденный узел при геометрической кратности 1.

![stable singular node](https://raw.re128.net/kogeletey/knw-media/diffequations/stable-singular-node.jpg)

![unstable singular node](https://raw.re128.net/kogeletey/knw-media/diffequations/unstable-singular-node.jpg)

### 4.  Если корни комплексные числа

![stable spiral anticlockwise](https://raw.re128.net/kogeletey/knw-media/diffequations/stable-spiral-anticlockwise.jpg)
![stable spiral clockwise](https://raw.re128.net/kogeletey/knw-media/diffequations/stable-spiral-clockwise.jpg)
![unstable spiral anticlockwise](https://raw.re128.net/kogeletey/knw-media/diffequations/unstable-spiral-anticlockwise.jpg)
![unstable spiral clockwise](https://raw.re128.net/kogeletey/knw-media/diffequations/unstable-spiral-clockwise.jpg)

### 5. Если корни только мнимые

![center clockwise](https://raw.re128.net/kogeletey/knw-media/diffequations/center-clockwise.jpg)
![center anticlockwise](https://raw.re128.net/kogeletey/knw-media/diffequations/center-anticlockwise.jpg)

### 6. Если корни $\lambda_1\neq 0,\lambda_2 = 0$

![singular case1](https://raw.re128.net/kogeletey/knw-media/diffequations/singular-case1.jpg)
![singular case2](https://raw.re128.net/kogeletey/knw-media/diffequations/singular-case2.jpg)

![singular case3](https://raw.re128.net/kogeletey/knw-media/diffequations/singular-case3.jpg)


<!-- ## На пример -->

