+++
title = "Examples physics problems"
date = "2021-07-12"
+++

1. Радиактивный распад
2. Уравнение движение матеральной точки
3. Уравнение колебания груза на пружине(1 интеграл)
   $$
   \frac{kx^2}{2}+{1\over 2}m\left(\frac{dx^2}{dt}\right)a^2=U=const\\\\
   kx\frac{dx}{dt}+m\frac{dx}{dx}\frac{d^2x}{dt^2}=0 \\\\
   \frac{d^2x}{dt^2}+\frac{k}{m}(=\omega^2)x=0
   $$
4. Колебания маятника
   $$
   \frac{y^2}{dt}+\omega^2_0\sin{y}=0\\\\
   \sin{y}\approx y\\\\
   $$
